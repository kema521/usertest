package services;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import dao.Employee;
import dao.ManageEmployee;
import dao.PasswordHash;
import dao.logAccount;


@Service("EmployeeService")
public class EmployeeService {

	@Autowired
	private ManageEmployee manager;
	
	//@Autowired
	//private PasswordEncoder passwordEncoder;

	public void setManager(ManageEmployee manager) {
		this.manager = manager;
	}
	
	// retrive all employees service
	public List<Employee> getEmployees() {
		//manager.connect();
		return manager.listEmployees();
	}
	
	
	// save data to database service
	public void createEmployee(Employee employee) {
		//manager.connect();
		manager.addEmployee(employee.getUsername(), employee.getPassword(), 
				employee.getEmail(),employee.getBirthday());
	}
	
	
	// update password for user
    public void updatePassword(Employee employee) {
		
		List<Employee> employees = this.getEmployees();
		for (Employee e:employees) {
			if(e.getUsername().equals(employee.getUsername())) {
				manager.OverridePassword(e.getId(),employee);
				System.out.println(e.getId());
			}
		}
	}
	
	
	
	
	// compare login data with database
	public boolean isUserValid(logAccount logger, boolean isName) {
		 
		List<Employee> employees = manager.listEmployees();
		if (isName) {
			for (Employee employee:employees) {
				if (logger.getAccount().equals(employee.getUsername())) {
					System.out.println("name: "+logger.getAccount());
					System.out.println("password: "+logger.getPassword());
					return isPasswdValid(logger.getPassword(),employee.getPassword());
				}	
			}
			
			return false;
			
		} else {
			for (Employee employee:employees) {
				if (logger.getAccount().equals(employee.getEmail())) {
					return isPasswdValid(logger.getPassword(),employee.getPassword());
				}
						
			}
			return false;
		}
   	 }
	
	
	
	public boolean isPasswdValid(String Login_password,String Hash_password) {
		
		boolean valid = false;
		try {
			valid = PasswordHash.validatePassword(Login_password, Hash_password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		
	    return valid;
		
	}
	
	
	
	
	// judge the uniqueness of email address
	public boolean isEmailUnique(Employee person) {
		
		List<Employee> employees = manager.listEmployees();
		for (Employee employee:employees) {
			if (person.getEmail().equals(employee.getEmail())) return false;
		} 
		
		return true;
	}
	
	
	// judge the uniqueness of username
	public boolean isUsernameUnique(Employee person) {
		
		List<Employee> employees = manager.listEmployees();
		for (Employee employee:employees) {
			if (person.getUsername().equals(employee.getUsername())) return false;
		} 
		
		return true;
	}
	
	

}
