package dao;


public class logAccount {
	
     private String account;
	 private String password; 
	 
	 public logAccount() {}
	 
	 public logAccount(String account, String passwd) {
	      this.account = account;
	      this.password = passwd;
	   }
	   
	  
	   public String getAccount() {
	      return account;
	   }
	   public void setAccount( String account ) {
	      this.account = account;
	   }
	   public String getPassword() {
	      return password;
	   }
	   
	   public void setPassword( String passwd ) {
	      this.password = passwd;
	   }

	  @Override
	  public String toString() {
		return "logAccount [account=" + account + ", password=" + password
				+ "]";
	}
	 
	 
	   
	   
	   
}
