package dao;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
// org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


//@Component("ManageEmployee")
public class ManageEmployee {
	
   @Autowired
   private SessionFactory factory; 
   

  // private static ServiceRegistry serviceRegistry;
   
  /* 
   public void connect() {
	   
	  
      try{
    	  Configuration configuration = new Configuration();
    	  configuration.configure("classpath:resources/hibernate_config/hibernate.cfg.xml");
    	  serviceRegistry = new ServiceRegistryBuilder().applySettings(
    	       configuration.getProperties()). buildServiceRegistry();
    	  factory = configuration.buildSessionFactory(serviceRegistry);
    	  
      }catch (Throwable ex) { 
         System.err.println("Failed to create sessionFactory object." + ex);
         throw new ExceptionInInitializerError(ex); 
      }
       
   }
   */
   
   /* Method to CREATE an employee in the database */
   public Integer addEmployee(String name, String passwd, String email, Date birthday){
      Session session = factory.openSession();
      Transaction tx = null;
      Integer employeeID = null;
      String myPassword = null;
      try{
         tx = session.beginTransaction();
         
         try {
			 myPassword = PasswordHash.createHash(passwd);
		 } catch (NoSuchAlgorithmException e) {
			 e.printStackTrace();
		 } catch (InvalidKeySpecException e) {
			 e.printStackTrace();
		 }
         Employee employee = new Employee(name,myPassword,email,birthday);
         employeeID = (Integer) session.save(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
      return employeeID;
   }
   
   
   public void OverridePassword(int id, Employee employee) {
		
	    String hashPassword = "";
		try {
			hashPassword = PasswordHash.createHash(employee.getPassword());
			System.out.println("password: "+employee.getPassword());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		
	    updateEmployee(id,hashPassword);
	}
   
   
   
   
   /* Method to  READ all the employees */
   public List<Employee> listEmployees( ){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         List<Employee> employees = session.createQuery("FROM Employee").list(); 
         tx.commit();
         return employees;
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
	return null;
   }
   
   
   
   /* Method to UPDATE password for an employee */
   public void updateEmployee(Integer EmployeeID, String password ){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Employee employee = (Employee)session.get(Employee.class, EmployeeID); 
         employee.setPassword(password);
         System.out.println(employee.getPassword());
		 session.update(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
   
   
   
   /* Method to DELETE an employee from the records */
   public void deleteEmployee(Integer EmployeeID){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Employee employee =  (Employee)session.get(Employee.class, EmployeeID); 
         session.delete(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
   
 }


