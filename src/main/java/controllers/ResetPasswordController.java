package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import services.EmployeeService;
import dao.Employee;

  

  @Controller
  public class ResetPasswordController {
	  
	@Autowired
	private EmployeeService helper;

	@RequestMapping(value="/passwdReset")
	public String resetPassword() {
		return "passwdReset";
	}
	
	
	@RequestMapping(value="/reset", method=RequestMethod.POST)
	public String newPasswordSubmit(Model model, Employee employee) {
		
		helper.updatePassword(employee);
		return "login";
	}
	
}
