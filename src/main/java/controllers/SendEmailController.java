package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import dao.Employee;
import dao.ManageEmployee;


@Controller
public class SendEmailController {
   
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private ManageEmployee manager;
	
	@RequestMapping(value="/sendEmail", method=RequestMethod.POST)
	public String doSendEmail(HttpServletRequest request) {
		
		// takes emailaddress from form
		String recipientAddress = request.getParameter("myemail");
		String subject = "Password reset.";
		
		// retrieve password, username from database
		List<String> info = retrievePassword(recipientAddress);
		String message = "hey! " + info.get(0) + "\n"+ "Here is a link you can reset your password!"+"\n";
		String link = "<a href=\"http://localhost:8080/usertest/passwdReset\">Password Reset safely!</a>";
	    String noHTMLString = new HtmlToPlainText().getPlainText(Jsoup.parse(link));
				
		
		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message+noHTMLString);
		
		// sends the e-mail
		mailSender.send(email);
		
		// forwords to the view "result"
		return "result";
	}
	
	
	public List<String> retrievePassword(String email) {
        List<String> user_info = new ArrayList<String>();
		//manager.connect();
		List<Employee> employees = manager.listEmployees();
		for (Employee employee: employees) {
			if (employee.getEmail().equals(email)) {
				user_info.add(employee.getUsername());
				break;
			}
		}
		
		return user_info;
	}

}
