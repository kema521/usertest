package controllers;


import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import javax.validation.Valid;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EmployeeService;
import dao.Employee;
import dao.logAccount;


@Controller
public class EmployeeController {
    
	@Autowired
	private EmployeeService helper;
	
	@RequestMapping("/Employees")          /* HTTP REQUEST="GET" retrieve data from mysql */
	public ModelAndView showEmployees() {
		ModelAndView mv = new ModelAndView("Employees");
		List<Employee> employees = helper.getEmployees();
		Map<String, Object> model = (Map) mv.getModel();
		model.put("employees", employees);
		return mv;
	}
	
	
	@RequestMapping(value="/register", method=RequestMethod.POST)    /* HTTP REQUEST="POST", insert data to mysql */  
	public String formSubmit(/*@ModelAttribute*/Model model, @Valid Employee employee, BindingResult result) {
		
		if (result.hasErrors()) {
			System.out.println("Form does not validate");
			return "createEmployee";
		} else {
			System.out.println("Form validate");
		} 
		 
		if (helper.isEmailUnique(employee) && helper.isUsernameUnique(employee)) {
			helper.createEmployee(employee);
			return "login";
		} else {
			
			if (!helper.isUsernameUnique(employee)) {
				model.addAttribute("message1", "try another username");
			}
			    
			if (!helper.isEmailUnique(employee)) {
				 model.addAttribute("message2", "try another email");
			}
			 
			return "createEmployee";
		}
		
	}
	
	
	@RequestMapping(value="/createEmployee", method=RequestMethod.GET)          /* HTTP REQUEST="GET" load register page*/
	public String turnRegister() {
		return "createEmployee";
	}
	
	
	@RequestMapping(value="/login", method=RequestMethod.POST)          /* HTTP REQUEST="POST" login page*/
	public String compareSql(@ModelAttribute logAccount logger, Model model) {
		if (isName_input(logger.getAccount())) {
			if (helper.isUserValid(logger, true)) return "success";
			else return "login";
		} else {
			if (helper.isUserValid(logger, false)) return "success";
			else return "login";
		}
	  }
		
	
	
	@RequestMapping(value="/mylogin", method=RequestMethod.GET)          /* HTTP REQUEST="GET" login page*/
	public String turnLogin() {
		return "login";
	}
	

	
	@RequestMapping(value="/email", method=RequestMethod.GET)          /* HTTP REQUEST="GET" email page*/
	public String email() {
		return "email";
	}
	
	
	/* check if the input is name or email */
	public boolean isName_input(String account) {
		if (account.contains("@")) return false;
		else return true;
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	    sdf.setLenient(true);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}
	
}

