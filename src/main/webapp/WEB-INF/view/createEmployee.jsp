<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sign up!</title>

   <link href="${pageContext.request.contextPath}/static/main.css" rel="stylesheet" type="text/css" >
   <link href="${pageContext.request.contextPath}/static/jquery.ui.all.css" rel="stylesheet" type="text/css">
   <link href="${pageContext.request.contextPath}/static/demos.css"  rel="stylesheet" type="text/css">
   <script src="${pageContext.request.contextPath}/static/jquery-1.5.1.js"></script>
   <script src="${pageContext.request.contextPath}/static/jquery.ui.core.js"></script>
   <script src="${pageContext.request.contextPath}/static/jquery.ui.widget.js"></script>
   <script src="${pageContext.request.contextPath}/static/jquery.ui.datepicker.js"></script>

   <script>
	$(function() {
		$( "#datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true
	    });
	});
	</script>

 <style>
    .error1 {
       font-size : 10px;
	   color: red;
    }
    
    .error2 {
       font-size : 10px;
	   color: red;
    }
    
 </style>


</head>
<body>

   <h1><b>Sign up here!</b></h1>
  
    <form name="myForm" action="${pageContext.request.contextPath}/register" method="post">
    <table class="mytable">
      <tr><td class="label">Name: </td><td><input name="username" type="text" /></td>
      <td><p class="error1" >${message1}</p></td></tr>
      <tr><td class="label">Email: </td><td><input id="Email" name="email" type="text"
      onchange="validate()" /></td><td><p id="demo" class="error2">${message2}</p></td></tr>
      
      
      <tr><td class="label">Email confirmation: </td><td><input id="Email1" name="emaiiConfrim" type="text"
      onchange="confirm()" /></td><td><p id="demo1"></p></td></tr>
      <tr><td class="label">Password: </td><td><input name="password" type="password" /></td></tr>
      <tr><td class="label">Birthday: </td><td><input  placeholder="click to show datepicker" id="datepicker"
      name="birthday" type="text" /></td></tr> 
      <tr><td class="label"><input type="submit" value="Submit"></td>
      
      <td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></td></tr>
      
    </table>
   </form>
   
   <script>
   
       
        function validate() {
        	var message= $("#Email").val();
            if (validateEmail(message)) {
        		document.getElementById('demo').innerHTML = "good!";
    
        	} else {
        		document.getElementById('demo').innerHTML = "email is not valid!";
        	}
      	
        }
        
        function validateEmail(email) {
        	var re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        	//var re = /.*\\@.*\\..*/;
        	return re.test(email);
        }
        
        function confirm() {
        	var email = $("#Email").val();
        	var email1 = $("#Email1").val();
        	
        	if (email1=="") document.getElementById('demo1').innerHTML = "";
        	else if (email===email1) {
        		document.getElementById('demo1').innerHTML = "good!";
        	} else {
        		document.getElementById('demo1').innerHTML = "not match, input again!";
        	}
        }
        
       
   </script>
      
</body>
</html>
