<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
    .mytable {
      padding:10px;
      border: 1px solid blue;
      background-color: #ccc;
   }
</style>

</head>
<body>
  
   <h1><b>Don't worry, just give a new password!</b></h1>
  
   <form name="Reset" action="${pageContext.request.contextPath}/reset" method="post">
     <table class="mytable">
      <tr><td>Name: </td><td><input name="username" type="text" /></td></tr>
      <tr><td>NewPassword: </td><td><input id="one" name="password" type="password" /></td></tr>
      <tr><td>NewPassword: </td><td><input id="again" name="Confirmpassword" onchange="verify()"
      type="password" /></td>
      <td><p id="demo"></p></td></tr>
      <tr><td><input type="submit" value="Reset"></td>
    </table>
   </form>
  
  
 <script>
 
    function verify() {
    	var one = $("#one").val();
        var again = $("#again").val();
        if (one===again) {
       	    document.getElementById("demo").innerHTML = "confirmed";
        } else {
        	document.getElementById("demo").innerHTML = "Not Match!";
        }
    }
     
 </script> 
  
</body>
</html>