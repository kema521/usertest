package dao;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Employee {
	   private int id;
	   
	   @NotNull
	   @Size(max=50)
	   private String username; 
	   
	   @NotNull
	   private String password; 
	   
	   @NotNull
	   @Pattern(regexp=".*\\@.*\\..*")
	   private String email;
	   
	   @NotNull
	   private Date birthday;

	   public Employee() {}
	   public Employee(String name, String passwd, String email ,Date birthday) {
	      this.username = name;
	      this.password = passwd;
	      this.email = email;
	      this.birthday = birthday;
	   }
	   public int getId() {
	      return id;
	   }
	   public void setId( int id ) {
	      this.id = id;
	   }
	   public String getUsername() {
	      return username;
	   }
	   public void setUsername( String name ) {
	      this.username = name;
	   }
	   public String getPassword() {
	      return password;
	   }
	   
	   public void setPassword( String passwd ) {
	      this.password = passwd;
	   }
	   
	   public String getEmail() {
		  return email;
	   }
	   
	   public void setEmail(String email) {
		   this.email = email;
	   }
	   
	   public Date getBirthday() {
	      return birthday;
	   }
	   
	   public void setBirthday(Date birthday) {
		  this.birthday = birthday;
	   }
	
	   
	   @Override
	   public String toString() {
		return "Employee [id=" + id + ", username=" + username + ", password="
				+ password + ", email=" + email + ", birthday=" + birthday
				+ "]";
	}
	   
}
	   
	   
	 
