<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>

<style type="text/css">
     .mytable {
      padding:10px;
      border: 1px solid blue;
   }
   
   input[type=text] {
       width: 150px;
   }
   
   input[type=password] {
       width:150px;
   }
</style>

</head>
<body>
    <h1><b>login!</b></h1>
    
    <form name="login" action="${pageContext.request.contextPath}/login" method="post">
    <table class="mytable">
      <tr><td>Name/Email: </td><td><input name="account" type="text" /></td></tr>
      <tr><td>Password: </td><td><input name="password" type="password" /></td></tr>
      <tr><td><input type="submit" value="log in"></td>
    </table>
   </form>
   
   
   <p><a href="${pageContext.request.contextPath}/email">forget password?</a></p>
  

</body>
</html>

